import { UserService } from './../../../user/services/user.service';
import { ConfirmModalComponent, REASONS } from './../../../../shared/components/confirm-modal/confirm-modal.component';
import { Restaurant } from './../../../../shared/interfaces/restaurant.interface';
import { RestaurantService } from './../../services/restaurant.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, forkJoin, of } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { finalize, catchError } from 'rxjs/operators';
import { formSanitizer } from '../../../../shared/helpers';

import 'leaflet/dist/leaflet';

const L = window['L'];
@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit, OnDestroy {
  @ViewChild('showDishModal') showDishModal: any;
  @ViewChild('addDishModal') addDishModal: any;

  public id: string;
  public restaurant: Restaurant;
  public dishes: any;

  private routeSub: Subscription;

  public editForm: FormGroup;
  public addForm: FormGroup;
  public filtersForm: FormGroup;

  public currentDish: any;
  public pending = false;
  public pendingFilter = false;

  constructor(
    private toastr: ToastrService,
    private userService: UserService,
    private route: ActivatedRoute,
    private restaurantService: RestaurantService,
    private modalService: NgbModal) { }

  public ngOnInit(): void {
    this.addForm = new FormGroup({
      name: new FormControl(''),
      category: new FormControl(''),
      description: new FormControl(''),
      note: new FormControl(''),
      price: new FormControl(''),
      allergies: new FormControl(''),
      images: new FormControl('')
    });

    this.filtersForm = new FormGroup({
      name: new FormControl(''),
      allergies: new FormControl('')
    });

    this.routeSub = this.route.params.subscribe(params => this.id = params['id']);

    let attributes = [];

    this.userService.getAllergies()
    .subscribe((allergies) => {
      attributes = allergies['data']['attributes'];

      this.filtersForm.patchValue({allergies: attributes['allergies']});
    });

    forkJoin([
        this.restaurantService.get({id: this.id}, 1),
        this.restaurantService.getDishes({restaurant: this.id, allergies: attributes['allergies']})
    ]).subscribe(results => {
      this.restaurant = results[0]['data'][0];
      this.dishes = results[1]['data'];

      this.showMap();
    });

  }

  public showMap(): void {
    const intervalID = setInterval(() => {
      if (document.getElementById('map')) {
        const lat = this.restaurant['attributes']['latitude'];
        const long = this.restaurant['attributes']['longitude'];

        const map = L.map('map').setView([lat, long], 18);

        L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
          attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
          minZoom: 10,
          maxZoom: 20
        }).addTo(map);

        L.marker([lat, long]).addTo(map)
          .bindPopup(this.restaurant['attributes']['name'])
          .openPopup();

        clearInterval(intervalID);
      }
    }, 1);
  }

  public open(content: any) {
    return this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', windowClass: 'add-dish-modal'} );
  }

  public filterDishes() {
    this.pendingFilter = true;

    this.restaurantService.getDishes({
      restaurant: this.id,
      ...formSanitizer(this.filtersForm),
    })
    .pipe(
      finalize(() => this.pendingFilter = false)
    )
    .subscribe(
      (res) => {
        this.dishes = res['data'];
      },
      (err) => this.toastr.error('Une erreur est survenue lors du filtrage', 'Oups..')
    );
  }

  public deleteDish() {
    this.open(ConfirmModalComponent).result.then((result) => {

    }, (reason) => {
      if (reason === REASONS.CONFIRM) {
        this.restaurantService.deleteDish(this.currentDish.id)
        .subscribe(
          (res) => {
            this.toastr.success('Le plat à bien été supprimé !', 'Merci !');

            this.modalService.dismissAll();

            this.restaurantService.getDishes({restaurant: this.id}).subscribe((dishes) => {
              this.dishes = dishes['data'];
            });
          },
          (err) => this.toastr.error('Une erreur est survenue', 'Oups..')
        );
      }
    });
  }

  private imgObjectToArray(array: any): string[] {
    const res = [];

    for (const index in array) {
      if (!!array[index]['url']) {
        res.push(array[index]['url'])
      }
    }

    return res;
  }

  public openDish(dish: any) {
    this.currentDish = dish;

    this.editForm = new FormGroup({
      name: new FormControl(this.currentDish['attributes']['name']),
      price: new FormControl(this.currentDish['attributes']['price']),
      note: new FormControl(this.currentDish['attributes']['note']),
      description: new FormControl(this.currentDish['attributes']['description']),
      allergies: new FormControl(this.currentDish['attributes']['allergies']),
      images: new FormControl(this.imgObjectToArray(this.currentDish['attributes']['images']))
    });

    this.open(this.showDishModal);
  }

  public editDish(): void {
    this.pending = true;

    this.restaurantService.editDish({
      restaurant: this.id as string,
      id: this.currentDish.id,
      ...formSanitizer(this.editForm)
    })
    .pipe(
      finalize(() => this.pending = false)
    )
    .subscribe(
      (res) => {
        this.toastr.success('Ta contribution à bien été prise en compte !', 'Merci !');

        this.modalService.dismissAll();

        this.editForm.reset();

        this.restaurantService.getDishes({restaurant: this.id}).subscribe((dishes) => {
          this.dishes = dishes['data'];
        });
      },
      (err) => {console.log(err); this.toastr.error('Une erreur est survenue', 'Oups..')}
    );
  }


  public addDish(): void {
    this.pending = true;

    this.restaurantService.addDish({
      restaurant: this.id as string,
      ...formSanitizer(this.addForm)
    })
    .pipe(
      finalize(() => this.pending = false)
    )
    .subscribe(
      (res) => {
        this.toastr.success('Ta contribution à bien été prise en compte !', 'Merci !');

        this.modalService.dismissAll();

        this.addForm.reset();

        this.restaurantService.getDishes({restaurant: this.id}).subscribe((dishes) => {
          this.dishes = dishes['data'];
        });
      },
      (err) => this.toastr.error('Une erreur est survenue lors de l\'ajout du plat', 'Oups ...')
    );
  }

  public ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

}
