import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';
import { StorageService } from './../../../shared/services/storage.service';
import { RestaurantService } from './../../restaurant/services/restaurant.service';
import { StoreService } from './../../../shared/services/store.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public pending = false;
  public addressForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private store: StoreService,
    private storageService: StorageService,
    private restaurantService: RestaurantService,
    private route: ActivatedRoute,
    private router: Router){

      this.addressForm = this.fb.group({
        address: [
          this.storageService.get('address') || '',
          [Validators.required, Validators.minLength(4)]
        ]
      });
    }

  ngOnInit(): void { }

  public onSubmit(): void {
    this.pending = true;

    const address = this.addressForm.get('address').value;
    this.storageService.set('address', address);

    this.restaurantService.get({place: address}, 5)
    .pipe(
      finalize(() => this.pending = false)
    )
    .subscribe(
      (restaurants) => {
        this.store.restaurants = restaurants;

        this.router.navigate(['restaurants'], { relativeTo: this.route });
      },
      (err) => {
        this.toastr.error('Une erreur est survenue', 'Oups !');
      }
    );

  }

}
