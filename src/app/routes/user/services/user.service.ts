import { StorageService } from './../../../shared/services/storage.service';
import { environment } from './../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http: HttpClient, public storageService: StorageService) { }

  public editProfile(infos: any): Observable<any> {
    const headers = new HttpHeaders(infos as any);

    return this.http.put(`${environment.apiUrl}/user`, { }, { headers });
  }

  public deleteProfile(): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/user`);
  }

  public refreshToken(): Observable<any> {
    return this.http.post(`${environment.apiUrl}/user/refreshToken`, { });
  }

  public getAllergies(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/user/allergies`);
  }

  public addAllergy(allergy: string | object): Observable<any> {
    const headers = new HttpHeaders(allergy as any);

    return this.http.post(`${environment.apiUrl}/user/allergies`, { }, { headers });
  }

  public deleteAllergy(allergy: string | object): Observable<any> {
    const allergyString = typeof allergy === 'object' ? allergy['value'] : allergy;

    const headers = new HttpHeaders({allergies: allergyString});

    return this.http.delete(`${environment.apiUrl}/user/allergies`, { headers });
  }

  public getTokenInfos() {
    const token = this.storageService.get('access_token');

    return require('jsonwebtoken').decode(token);
  }
}
