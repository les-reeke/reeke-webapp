export interface User {
    login: string;
    password: string;
    firstname: string;
    lastname: string;
    email: string;
    home: string;
    work: string;
}
