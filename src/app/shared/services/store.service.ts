import { Injectable } from '@angular/core';
import { Restaurant } from '../interfaces/restaurant.interface';
import { User } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  public restaurants: Restaurant[];

  public user: User;

  constructor() { }
}
