import { UserService } from './../../../routes/user/services/user.service';
import { StoreService } from './../../services/store.service';
import { StorageService } from './../../services/storage.service';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';
import { AuthService } from './../../services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent {
  @ViewChild('modal') modal: any;

  public pending = false;
  public signInForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private modalService: NgbModal,
    private storageService: StorageService,
    private store: StoreService,
    private authService: AuthService,
    private toastr: ToastrService) {
    this.signInForm = this.fb.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  public open() {
    this.modalService.open(this.modal);
  }

  public signIn() {
    this.pending = true;

    this.authService.signIn(this.signInForm.value)
      .pipe(
        finalize(() => this.pending = false)
      )
      .subscribe(
        (res) => {
          this.modalService.dismissAll();

          const { access_token } = res['data']['attributes'];

          this.storageService.set('access_token', access_token);

          this.store.user = JSON.parse(this.userService.getTokenInfos()['user'] || null);

          this.signInForm.reset();
        },
        (err) => this.toastr.error('Mauvais couple login/mot de passe', 'Oups..')
      );
  }

}
