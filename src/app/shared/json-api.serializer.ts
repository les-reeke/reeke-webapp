export function filterSerializer(data: object): string {
    let queryString = '';
    let index = 0;

    Object.keys(data).forEach(key => {
        if (!!data[key]) {
            queryString += !index ? '?' : '&';
            queryString += `filter[${key}]=${data[key]}`;
            ++index;
        }
    });

    return encodeURI(queryString);
}
