import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

export enum REASONS {
  CONFIRM = 'CONFIRM',
  CANCEL = 'CANCEL',
}

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent {
  public reasons = REASONS;

  constructor(public activeModal: NgbActiveModal) { }
}
