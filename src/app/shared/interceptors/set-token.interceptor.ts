import { StorageService } from './../services/storage.service';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class SetTokenInterceptor implements HttpInterceptor {

  constructor(private storageService: StorageService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    if (this.storageService.has('access_token')){
      request = request.clone({
        setHeaders: {
          token: this.storageService.get('access_token')
        }
      });
    }

    return next.handle(request);
  }
}
