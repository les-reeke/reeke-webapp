import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent {
  @ViewChild('modal') modal: any;

  public pending = false;
  public signUpForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private authService: AuthService,
    private toastr: ToastrService) {
    this.signUpForm = this.fb.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      home: [''],
      work: [''],
    });
  }

  public open() {
    this.modalService.open(this.modal);
  }

  public signUp() {
    this.pending = true;

    this.authService.signUp(this.signUpForm.value)
      .pipe(
        finalize(() => this.pending = false)
      )
      .subscribe(
        (res) => {
          this.modalService.dismissAll();

          this.toastr.success('Inscription réussie', 'Yes !');

          this.signUpForm.reset();
        },
        (err) => {
          this.toastr.error('Inscription echouée', 'Oups..');
        }
      );
  }
}
