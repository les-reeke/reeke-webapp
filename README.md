# Requis

Il est nécessaire d'avoir NodeJS >= 8.9 et NPM >= 5.5.1 d'installer sur votre machine.

## Lancer en développement ( local )

Dans le projet, lancer `npm install` afin d'installer les packages NPM et avoir accès au CLI d'Angular.

Une fois terminé, lancer `npm run start` afin de lancer le serveur local, par défaut il sera sur `http://localhost:4200/`.

## Build

Lancer `npm run build` pour build le projet. Le build va ensuite être créé dans le dossier `dist` à la racine.
