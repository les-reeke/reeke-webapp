export interface Restaurant {
    id: number;
    type: string;
    attributes: object;
    geometry: object;
}
