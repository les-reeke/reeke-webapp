import { AuthService } from './../../../shared/services/auth.service';
import { StorageService } from './../../../shared/services/storage.service';
import { ConfirmModalComponent, REASONS } from './../../../shared/components/confirm-modal/confirm-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StoreService } from './../../../shared/services/store.service';
import { User } from './../../../shared/interfaces/user.interface';
import { ToastrService } from 'ngx-toastr';
import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { tagInputToArray, formSanitizer } from '../../../shared/helpers';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public pending = false;
  public pendingDelete = false;
  public disableAllergy = false;
  private user: User;
  private allergies: any;
  public form: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private storageService: StorageService,
    private modalService: NgbModal,
    private userService: UserService,
    private authService: AuthService,
    private store: StoreService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.user = this.store.user;

    this.form = this.fb.group({
      firstname: [this.user['firstname']],
      lastname: [this.user['lastname']],
      password: [''],
      allergies: [''],
      email: [this.user['email']],
      home: [this.user['home']],
      work: [this.user['work']],
    });

    this.userService.getAllergies().subscribe((allergies) => {
      this.allergies = allergies['data']['attributes'];

      this.form.patchValue({allergies: this.allergies['allergies']});
    });
  }

  public deleteAccount() {
    this.modalService.open(ConfirmModalComponent).result.then((result) => {

    }, (reason) => {
      if (reason === REASONS.CONFIRM) {
        this.pendingDelete = true;

        this.userService.deleteProfile()
        .pipe(
          finalize(() => this.pendingDelete = false)
        )
        .subscribe(
          (res) => {
            this.authService.signOut();

            this.router.navigate(['/restaurants']);

            this.toastr.success('Ton compte à bien été supprimé !', 'Aurevoir !');

            this.modalService.dismissAll();
          },
          (err) => this.toastr.error('Une erreur est survenue lors de la suppression de votre compte', 'Oups..')
        );
      }
    });
  }

  public addAllergy(allergy: string) {
    this.disableAllergy = true;

    this.userService.addAllergy({allergies: tagInputToArray([allergy])})
    .pipe(
      finalize(() => this.disableAllergy = false)
    ).subscribe(
      (res) => this.toastr.success('L\'allergie à bien été ajoutée'),
      (err) => this.toastr.error('Une erreur est survenue lors de l\'ajout d\'une allergie', 'Oups..')
    );
  }

  public deleteAllergy(allergy: string) {
    this.disableAllergy = true;

    this.userService.deleteAllergy(allergy)
    .pipe(
      finalize(() => this.disableAllergy = false)
    ).subscribe(
      (res) => this.toastr.success('L\'allergie à bien été supprimée'),
      (err) => this.toastr.error('Une erreur est survenue lors de la suppression d\'une allergie', 'Oups..')
    );
  }

  public refreshToken() {
    this.userService.refreshToken().subscribe((res) => {
      const { access_token } = res['data']['attributes'];

      this.storageService.set('access_token', access_token);

      this.store.user = JSON.parse(this.userService.getTokenInfos()['user'] || null);
    });
  }

  public onSubmit() {
    this.pending = true;

    this.userService.editProfile({
      ...formSanitizer(this.form)
    })
    .pipe(
      finalize(() => this.pending = false)
    )
    .subscribe(
      (res) => {
        this.refreshToken();

        this.toastr.success('Tes informations ont bien été modifiées !', 'Hey !');
      },
      (err) => this.toastr.error('Une erreur est survenue lors de la modification de vos données', 'Oups..')
    );
  }

}
