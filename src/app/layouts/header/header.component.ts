import { RestaurantService } from './../../routes/restaurant/services/restaurant.service';
import { StoreService } from './../../shared/services/store.service';
import { AuthService } from './../../shared/services/auth.service';
import { StorageService } from './../../shared/services/storage.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public form: FormGroup;
  public signUpForm: FormGroup;

  public displayAddressInput = false;
  public pending = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    public store: StoreService,
    private restaurantService: RestaurantService,
    private storageService: StorageService) {
    this.router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this.displayAddressInput = (val.url === '/restaurants');
      }
    });

    this.form = new FormGroup({
      address: new FormControl(this.storageService.get('address') || ''),
    });
  }

  ngOnInit(): void { }

  public changeAddress() {
    const address = this.form.get('address').value;

    this.storageService.set('address', address);

    this.restaurantService.get({place: address}, 5)
    .subscribe(
      (restaurants) => {
        this.store.restaurants = restaurants;
      },
      (err) => {},
    );
  }

  public onSubmit(): void {
    this.form.get('address');
  }

  public logout() {
    this.authService.signOut();

    this.router.navigate(['/']);
  }

}
