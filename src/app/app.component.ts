import { UserService } from './routes/user/services/user.service';
import { Component, OnInit } from '@angular/core';
import { StoreService } from './shared/services/store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private store: StoreService, private userService: UserService) {}

  ngOnInit(): void {
    this.store.user = !!this.userService.getTokenInfos() ? JSON.parse(this.userService.getTokenInfos()['user']) : null;
  }
}
