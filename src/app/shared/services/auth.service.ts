import { ToastrService } from 'ngx-toastr';
import { StoreService } from './store.service';
import { StorageService } from './storage.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
interface SignInCredential {
  login: string;
  password: string;
}

interface SignUpCredential {
  login: string;
  password: string;
  firstname: string;
  lastname: string;
  email: string;
  home: string;
  work: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public http: HttpClient,
    private storageService: StorageService,
    private store: StoreService,
    private toastr: ToastrService) { }

  public signIn(credentials: SignInCredential): Observable<any> {
    const headers = new HttpHeaders(credentials as any);

    return this.http.post(`${environment.apiUrl}/user/login`, {}, { headers });
  }

  public signUp(credentials: SignUpCredential): Observable<any> {
    const headers = new HttpHeaders(credentials as any);

    return this.http.post(`${environment.apiUrl}/user`, {}, { headers });
  }

  public signOut(): void {
    this.storageService.remove('access_token');

    if (this.storageService.has('access_token')) {
      this.toastr.error('Erreur lors de la déconnexion');
    } else {
      this.store.user = null;

      this.toastr.success('Vous avez bien été déconnecté');
    }
  }

}
