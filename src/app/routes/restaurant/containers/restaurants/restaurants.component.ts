import { finalize } from 'rxjs/operators';
import { StorageService } from './../../../../shared/services/storage.service';
import { StoreService } from './../../../../shared/services/store.service';
import { RestaurantService } from '../../services/restaurant.service';
import { Restaurant } from '../../../../shared/interfaces/restaurant.interface';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.scss']
})
export class RestaurantsComponent implements OnInit {
  public restaurants: Restaurant[];
  public nextPage: string;
  public pendingLoadMore = false;

  public filtersForm: FormGroup;

  public perPage = 5;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private store: StoreService,
    private storageService: StorageService,
    private restaurantService: RestaurantService,
  ) { }

  ngOnInit(): void {
    this.filtersForm = this.fb.group({
      name: ['']
    });

    if (!this.store.restaurants) {
      this.getRestaurants();
    } else {
      this.restaurants = this.store.restaurants['data'];

      this.nextPage = this.store.restaurants['links']['next'];
    }
  }

  public goTo(route: string) {
    this.router.navigate([route], { relativeTo: this.route });
  }

  public getRestaurants() {
    const address = this.storageService.get('address');

    this.restaurantService.get({place: address, ...this.filtersForm.value}, this.perPage)
    .subscribe(
      (restaurants) => {
        this.restaurants = restaurants['data'];

        this.nextPage = restaurants['links']['next'];
      },
      (err) => {},
    );
  }

  public loadMore() {
    const address = this.storageService.get('address');

    this.pendingLoadMore = true;

    this.restaurantService.getPage(this.nextPage, this.perPage)
    .pipe(
      finalize(() => this.pendingLoadMore = false)
    )
    .subscribe(
      (restaurants) => {
        this.restaurants = [...this.restaurants, ...restaurants['data']];

        this.nextPage = restaurants['links']['next'];
      },
      (err) => {},
    );
  }

}
