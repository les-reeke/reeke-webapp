import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsAuthenticatedGuard } from './shared/guards/is-authenticated.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./routes/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'restaurants',
    loadChildren: () => import('./routes/restaurant/restaurant.module').then(m => m.RestaurantModule),
  },
  {
    path: 'mon-compte',
    loadChildren: () => import('./routes/user/user.module').then(m => m.UserModule),
    canActivate: [IsAuthenticatedGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
