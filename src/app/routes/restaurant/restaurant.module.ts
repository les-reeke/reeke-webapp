import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantsComponent } from './containers/restaurants/restaurants.component';
import { RestaurantComponent } from './containers/restaurant/restaurant.component';

const routes: Routes = [
  {
    path: '',
    component: RestaurantsComponent
  },
  {
    path: ':id',
    component: RestaurantComponent
  }
];

@NgModule({
  declarations: [
    RestaurantsComponent,
    RestaurantComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class RestaurantModule { }
