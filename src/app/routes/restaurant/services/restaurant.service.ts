import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { filterSerializer } from '../../../shared/json-api.serializer';
@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(public http: HttpClient) { }

  public get(filters: any, perPage: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/restaurants${filterSerializer(filters)}&page%5Bsize%5D=${perPage}`);
  }

  public getPage(url: string, perPage: number) {
    return this.http.get(`${url}&page%5Bsize%5D=${perPage}`);
  }

  public getDishes(filters: any): Observable<any> {
    return this.http.get(`${environment.apiUrl}/restaurants/dishes${filterSerializer(filters)}`);
  }

  public addDish(dish: any): Observable<any> {
    const headers = new HttpHeaders(dish as any);

    return this.http.post(`${environment.apiUrl}/restaurants/dishes`, { }, { headers });
  }

  public deleteDish(id: any): Observable<any> {
    const params = new HttpParams().set('id', id);

    return this.http.delete(`${environment.apiUrl}/restaurants/dishes`, { params });
  }

  public editDish(data: any): Observable<any> {
    const headers = new HttpHeaders(data as any);

    return this.http.put(`${environment.apiUrl}/restaurants/dishes`, { }, { headers });
  }
}
