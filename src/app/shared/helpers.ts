export function tagInputToArray(tagInputValues: object | string[]): string[] {
    const array = [];

    for (const index in tagInputValues) {
      if (!!tagInputValues[index].value || typeof tagInputValues[index] === 'string') {
        array.push(tagInputValues[index].value || tagInputValues[index]);
      }
    }

    return array;
}

export function formSanitizer(form: any) {
  let formSanitized = {};

  for (const param in form.value) {
    if (!!form.get(param).value) {

      if (form.get(param).value instanceof Array && form.get(param).value.length === 0) {
        break;
      }

      let original = form.get(param).value;

      if (param === 'allergies' || param === 'images') {
        original = tagInputToArray(form.get(param).value);
      }

      const value = typeof original === 'number'
        ? (original).toString()
        : original;

      formSanitized = { ...formSanitized, [param]: value };
    }
  }

  return formSanitized;
}
