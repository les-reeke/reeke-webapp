import { ToastrService } from 'ngx-toastr';
import { StorageService } from './../services/storage.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IsAuthenticatedGuard implements CanActivate {
  constructor(private toastr: ToastrService, private storageService: StorageService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (!this.storageService.get('access_token')) {
        this.toastr.error('Vous devez être connecté pour accéder à cette page', 'Oups !');

        this.router.navigate(['/']);

        return false;
      }

      return true;
  }
}
