export const environment = {
  production: true,
  stubUrl: 'https://api.partagetesco.fr/v-bouchons',
  apiUrl: 'https://api.partagetesco.fr/v-latest',
};
